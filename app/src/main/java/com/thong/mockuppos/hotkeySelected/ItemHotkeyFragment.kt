package com.thong.mockuppos.hotkeySelected


import android.app.Activity.RESULT_OK
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.thong.mockuppos.R
import com.thong.mockuppos.hotkey.Hotkey
import com.thong.mockuppos.itemSubgroup.ItemSubgroupActivity
import kotlinx.android.synthetic.main.fragment_list_item_hotkey.view.*


class ItemHotkeyFragment : Fragment(), HotkeySelectAdapter.HotkeySelectAdapterCallback {

    private lateinit var hotkeySelectAdapter: HotkeySelectAdapter
    var listSelectCallback: ListSelectCallback? = null
    private var hotkey: Hotkey? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        hotkeySelectAdapter = HotkeySelectAdapter(context!!)
        hotkeySelectAdapter.hotkeySelectAdapterCallback = this
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_list_item_hotkey, container, false)

        setRecyclerView(view)

        return view
    }

    override fun itemSelected(`objects`: Any) {
        super.itemSelected(`objects`)
        Log.e("Selected", `objects`.toString())
        listSelectCallback?.itemSelected(`objects`)
    }

    override fun selectItem(position: Int) {
        super.selectItem(position)
        hotkey = setData()[position]

        val bundle = Bundle()
        bundle.putSerializable("sub_group",hotkey!!)
        val intent = Intent(context,ItemSubgroupActivity::class.java)
        intent.putExtras(bundle)
        startActivityForResult(intent,1000)
    }

    private fun setRecyclerView(view: View) {
        view.hotkeyRecyclerView.apply {
            layoutManager = LinearLayoutManager(context!!)
            hotkeySelectAdapter.mValues = setData()
            adapter = hotkeySelectAdapter
        }
    }

    private fun setData(): ArrayList<Hotkey> {
        val lists: ArrayList<Hotkey> = ArrayList()
        for (i in 0..4) {
            val hotkey = Hotkey()
            when (i) {
                0 -> {
                    hotkey.title = "ผักบุ้ง"
                    hotkey.img_url =
                        "http://sukkaphap-d.com/wp-content/uploads/2016/07/Morning-glory.jpg"
                }
                1 -> {
                    hotkey.title = "ปลาทู"
                    hotkey.img_url =
                        "http://kroobannok.com/news_pic/p71804210810.jpg"
                }
                2 -> {
                    hotkey.title = "เนื้อหมู"
                    hotkey.img_url =
                        "https://hilight.kapook.com/img_cms2/user/thitima/New/dfjr_22g6625.jpg"
                }
                3 -> {
                    hotkey.title = "อกไก่่"
                    hotkey.img_url =
                        "https://secure.ap-tescoassets.com/assets/TH/996/214996/ShotType1_540x540.jpg"
                }
                4 -> {
                    hotkey.title = "น้ำตาลทรายครึ่งโล"
                    hotkey.img_url =
                        "https://shop-api.readyplanet.com/v1/image/500x0/9e6028a986c2477297dd765e458b3331"
                }
            }

            lists.add(hotkey)
        }
        return lists
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == 1000){
            if (resultCode == RESULT_OK){
                val bundle = data?.extras
                val data = bundle?.getSerializable("data") as Hotkey?
                listSelectCallback?.itemSelected(data!!)
            }

        }
    }

    interface ListSelectCallback {
        fun itemSelected(`objects`: Any) {}
    }
}
