package com.thong.mockuppos.hotkeySelected

import android.content.Context
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.view.setPadding
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.thong.mockuppos.R
import com.thong.mockuppos.hotkey.Hotkey
import kotlinx.android.synthetic.main.item_hotkey_add.view.*
import org.zakariya.stickyheaders.SectioningAdapter
import java.util.*
import kotlin.collections.ArrayList


class HotkeySelectAdapter(val context: Context) :
    RecyclerView.Adapter<HotkeySelectAdapter.ViewHolder>() {

    var mValues: List<Hotkey> = ArrayList()
    var hotkeySelectAdapterCallback: HotkeySelectAdapterCallback? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_hotkey_add, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = mValues[position]
        holder.view.textTitle.text = item.title

        Glide.with(context)
            .load(item.img_url)
            .transform(CenterCrop())
            .into(holder.view.iconImage)

        holder.itemView.setOnClickListener {
            hotkeySelectAdapterCallback?.selectItem(position)
        }

        holder.view.textSelect.setOnClickListener {
            hotkeySelectAdapterCallback?.itemSelected(item)
        }
    }

    override fun getItemCount(): Int = mValues.size

    inner class ViewHolder(mView: View) : RecyclerView.ViewHolder(mView) {
        val view = mView
    }

    interface HotkeySelectAdapterCallback {
        fun itemSelected(`objects`: Any) {}
        fun selectItem(position: Int){}
    }
}