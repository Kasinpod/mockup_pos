package com.thong.mockuppos.hotkeySelected


import android.app.Dialog
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.iammert.library.AnimatedTabLayout
import com.thong.mockuppos.CustomPagerAdapter
import com.thong.mockuppos.R
import kotlinx.android.synthetic.main.bottom_sheet_hot_key.*
import kotlinx.android.synthetic.main.bottom_sheet_hot_key.view.*

class BottomHotkeySelect : BottomSheetDialogFragment(),ItemHotkeyFragment.ListSelectCallback,
    AddHotkeyFragment.AddHotkeyFragmentCallback {

    var bottomHotkeySelectCallback: BottomHotkeySelectCallback? = null

    private lateinit var listItemHotkeyFragment :ItemHotkeyFragment
    private lateinit var addHotkeyFragment: AddHotkeyFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        listItemHotkeyFragment = ItemHotkeyFragment()
        listItemHotkeyFragment.listSelectCallback = this
        addHotkeyFragment = AddHotkeyFragment()
        addHotkeyFragment.addHotkeyFragmentCallback = this
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val bottomSheetView = inflater.inflate(R.layout.bottom_sheet_hot_key, container, false)

        setUpTabBar(bottomSheetView)

        return bottomSheetView
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val bottomSheetDialog = super.onCreateDialog(savedInstanceState) as BottomSheetDialog
        bottomSheetDialog.setOnShowListener {
            val dialog = it as BottomSheetDialog
            val bottomSheet =
                dialog.findViewById<View>(com.google.android.material.R.id.design_bottom_sheet)
            val bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet)
            bottomSheetBehavior.setBottomSheetCallback(object :
                BottomSheetBehavior.BottomSheetCallback() {
                override fun onSlide(bottomSheet: View, slideOffset: Float) {
                    Log.e("bottomSheet", "onSlide$bottomSheet")
                    Log.e("slideOffset", "onSlide$slideOffset")
                }

                override fun onStateChanged(bottomSheet: View, newState: Int) {
                    Log.e("bottomSheet", "onStateChanged$bottomSheet")
                    Log.e("slideOffset", "onStateChanged$newState")

                    when (newState) {
                        BottomSheetBehavior.STATE_EXPANDED -> {
                            bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
                        }
                        BottomSheetBehavior.STATE_HALF_EXPANDED -> {
                            bottomSheetBehavior.state = BottomSheetBehavior.STATE_HALF_EXPANDED
                        }
                        BottomSheetBehavior.STATE_HIDDEN -> {
                            dismiss()
                        }
                        else -> {
                        }
                    }
                }
            })

        }
        return bottomSheetDialog
    }

    override fun onStart() {
        super.onStart()
        dialog?.also {
            val bottomSheet = dialog?.findViewById<View>(R.id.design_bottom_sheet)
            bottomSheet?.layoutParams?.height = ViewGroup.LayoutParams.MATCH_PARENT
            val behavior = BottomSheetBehavior.from<View>(bottomSheet)
            content.viewTreeObserver?.addOnGlobalLayoutListener(object :
                ViewTreeObserver.OnGlobalLayoutListener {
                override fun onGlobalLayout() {
                    try {
                        content.viewTreeObserver.removeGlobalOnLayoutListener(this)
                        behavior.peekHeight = content.height
                        view?.requestLayout()
                    } catch (e: Exception) {
                    }
                }
            })
        }
    }


    override fun itemSelected(`objects`: Any) {
        super.itemSelected(`objects`)
        bottomHotkeySelectCallback?.itemSelected(`objects`)
        this.dismiss()
    }

    override fun selectedAddItemSelected(`objects`: Any) {
        super.selectedAddItemSelected(`objects`)
        bottomHotkeySelectCallback?.itemSelected(`objects`)
    }


    private fun setUpTabBar(bottomSheetView: View) {
        val fragments: ArrayList<Fragment> = ArrayList()
        fragments.add(listItemHotkeyFragment)
//        fragments.add(addHotkeyFragment)

        val adapter = CustomPagerAdapter(fragments,childFragmentManager)
        bottomSheetView.hotkeyPageView.adapter = adapter
        bottomSheetView.tabbarBottomsheet.setupViewPager(bottomSheetView.hotkeyPageView)
        bottomSheetView.tabbarBottomsheet.setTabChangeListener(object :
            AnimatedTabLayout.OnChangeListener {
            override fun onChanged(position: Int) {
                bottomSheetView.tabbarBottomsheet.background =
                    ContextCompat.getDrawable(context!!, R.color.colorWhite)

            }
        })
    }

    interface BottomHotkeySelectCallback {
        fun itemSelected(`objects`: Any) {}
    }
}