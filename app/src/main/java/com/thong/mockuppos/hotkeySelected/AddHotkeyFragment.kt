package com.thong.mockuppos.hotkeySelected


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.thong.mockuppos.R
import com.thong.mockuppos.hotkey.Hotkey
import kotlinx.android.synthetic.main.fragment_add_hotkey.view.*

class AddHotkeyFragment : Fragment() {

    var addHotkeyFragmentCallback: AddHotkeyFragmentCallback? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view= inflater.inflate(R.layout.fragment_add_hotkey, container, false)

        onEvent(view)

        return view
    }

    private fun onEvent(view: View) {
        view.confirmAddHotkey.setOnClickListener {
            setData()
        }
    }

    private fun setData() {
        val hotkey = Hotkey()
        hotkey.title = ""
        hotkey.img_url = "https://www.cowgirlcontractcleaning.com/wp-content/uploads/sites/360/2018/05/placeholder-img-4.jpg"
        addHotkeyFragmentCallback?.selectedAddItemSelected(hotkey)
    }

    interface AddHotkeyFragmentCallback {
        fun selectedAddItemSelected(`objects`: Any) {}
    }
}
