package com.thong.mockuppos.bill

import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.thong.mockuppos.R
import com.thong.mockuppos.bill.dummy.BottomSheetEditPriceItem
import com.thong.mockuppos.bill.dummy.DummyContent
import com.thong.mockuppos.bill.dummy.DummyContent.DummyItem
import com.thong.mockuppos.utility.swipe.SwipeController
import com.thong.mockuppos.utility.swipe.SwipeHelper
import kotlinx.android.synthetic.main.fragment_bill_list.view.*

class BillFragment : Fragment() {

    private lateinit var mybillRecyclerViewAdapter: MybillRecyclerViewAdapter
    private lateinit var viewManager: RecyclerView.LayoutManager
    private lateinit var colorDrawableBackground: ColorDrawable
    private lateinit var colorDrawableBackgroundEdit: ColorDrawable
    private lateinit var deleteIcon: Drawable
    private lateinit var editIcon: Drawable
    private var fabExpanded = false

    private lateinit var swipeController: SwipeController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mybillRecyclerViewAdapter = MybillRecyclerViewAdapter(this.context!!)
        swipeController = SwipeController()

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_bill_list, container, false)
        setupRecyclerView(view)
        onEvent(view)
        return view
    }


    private fun setupRecyclerView(view: View) {
        mybillRecyclerViewAdapter.mValues = DummyContent.ITEMS
        view.list.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context)
            adapter = mybillRecyclerViewAdapter
            addItemDecoration(
                (DividerItemDecoration(
                    context!!,
                    DividerItemDecoration.VERTICAL
                ))
            )
        }

//        swipeController.setOnSelectSwipe(object : SwipeControllerActions {
//            override fun onRightClicked(position: Int) {
//                mybillRecyclerViewAdapter.notifyItemRemoved(position)
//            }
//        })
//
//        val itemTouchHelper = ItemTouchHelper(swipeController)
//        itemTouchHelper.attachToRecyclerView(view.list)
//
//        view.list.addItemDecoration(object : RecyclerView.ItemDecoration() {
//            override fun onDraw(c: Canvas, parent: RecyclerView, state: RecyclerView.State) {
//                super.onDraw(c, parent, state)
//                swipeController.onDraw(c)
//            }
//        })
    }

    private fun onEvent(view: View) {
        val swipeHelper = object : SwipeHelper(context!!, view.list) {
            override fun instantiateUnderlayButton(
                viewHolder: RecyclerView.ViewHolder,
                underlayButtons: MutableList<UnderlayButton>
            ) {
                underlayButtons.add(
                    UnderlayButton(
                        ShowType.show_icon,
                        "Delete",
                        0,
                        setIcon(R.drawable.ic_delete,"#FFFFFF"),
                        Color.parseColor("#FF3C30"),
                        object : UnderlayButtonClickListener {
                            override fun onClick(pos: Int) {
                                Toast.makeText(context,"Delete",Toast.LENGTH_SHORT).show()
                                view.list.adapter!!.notifyItemRemoved(pos)
                            }

                        }
                    )
                )

                underlayButtons.add(
                    UnderlayButton(
                        ShowType.show_icon,
                        "Update",
                        0,
                        setIcon(R.drawable.ic_pencel,"#FFFFFF"),
                        Color.parseColor("#FF9502"),
                        object : UnderlayButtonClickListener {
                            override fun onClick(pos: Int) {
                                val bottomSheetEditQtyItem = BottomSheetEditQtyItem()
                                bottomSheetEditQtyItem.show(
                                    activity!!.supportFragmentManager,
                                    BottomSheetEditQtyItem::class.java.simpleName
                                )
                                view.list.adapter!!.notifyDataSetChanged()
                            }

                        }
                    )
                )

                underlayButtons.add(
                    UnderlayButton(
                        ShowType.show_icon,
                        "Info",
                        0,
                        setIcon(R.drawable.ic_bill,"#FFFFFF"),
                        Color.parseColor("#7C7C7C"),
                        object : UnderlayButtonClickListener {
                            override fun onClick(pos: Int) {
                                val bottomSheetEditPriceItem = BottomSheetEditPriceItem()
                                bottomSheetEditPriceItem.show(
                                    activity!!.supportFragmentManager,
                                    BottomSheetEditPriceItem::class.java.simpleName
                                )
                                view.list.adapter!!.notifyDataSetChanged()
                            }

                        }
                    )
                )
            }

        }

        view.fab.setOnClickListener {
            when {
                fabExpanded -> {
                    closeSubMenusFab(view)
                }
                else -> {
                    openSubMenusFab(view)
                }
            }
        }

        view.list.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)

                when {
                    dy > 0 && view.fab.visibility == View.VISIBLE -> {
                        view.fab.visibility = View.GONE
                        view.buttonProcess.visibility = View.GONE
                        val animation: Animation = AnimationUtils.loadAnimation(
                            this@BillFragment.context!!,
                            R.anim.slide_to_bottom
                        )
                        if(view.fabCancelBill.isShown){
                            closeSubMenusFab(view)
                        }
                        view.fab.animation = animation
                        view.buttonProcess.animation = animation
                    }
                    dy < 0 && view.fab.visibility != View.VISIBLE -> {
                        view.fab.visibility = View.VISIBLE
                        view.buttonProcess.visibility = View.VISIBLE
                        val animation: Animation = AnimationUtils.loadAnimation(
                            this@BillFragment.context!!,
                            R.anim.slide_to_top
                        )
                        view.fab.animation = animation
                        view.buttonProcess.animation = animation
                    }
                }
            }
        })

        view.buttonProcess.setOnClickListener {
            openBottomSheetProcessSummary()
        }

    }

    private fun openBottomSheetProcessSummary() {
        val bottomSheetSummaryRetailSale = BottomSheetSummaryRetailSale()
        bottomSheetSummaryRetailSale.show(
            activity!!.supportFragmentManager,
            BottomSheetSummaryRetailSale::class.java.simpleName
        )
    }

    fun closeSubMenusFab(view: View) {
        slideToBottom(view.fabCancelBill,view.textViewCancelBill,20)
        slideToBottom(view.fabCloseBill,view.textViewCloseBill,15)
        slideToBottom(view.fabClosedBill,view.textViewClosedBill,10)
        slideToBottom(view.fabOpenedBill,view.textViewOpenedBill,0)
        fabExpanded = false
    }

    //Opens FAB submenus
    fun openSubMenusFab(view: View) {
        slideToTop(view.fabCancelBill,view.textViewCancelBill,0)
        slideToTop(view.fabCloseBill,view.textViewCloseBill,10)
        slideToTop(view.fabClosedBill,view.textViewClosedBill,15)
        slideToTop(view.fabOpenedBill,view.textViewOpenedBill,20)
        fabExpanded = true
    }

    private fun slideToTop(view: View,view2: View,time: Long) {
        val handler = Handler()
        handler.postDelayed(Runnable {
            view.visibility = View.VISIBLE
            view2.visibility = View.VISIBLE
            val animation: Animation = AnimationUtils.loadAnimation(
                this.context!!,
                R.anim.slide_to_top
            )
            view.animation = animation
            view2.animation = animation
        }, time)
    }

    private fun slideToBottom(view: View,view2: View,time: Long) {
        val handler = Handler()
        handler.postDelayed(Runnable {
            view.visibility = View.GONE
            view2.visibility = View.GONE
            val animation: Animation = AnimationUtils.loadAnimation(
                this.context!!,
                R.anim.slide_to_bottom
            )
            view.animation = animation
            view2.animation = animation
        }, time)
    }

    private fun setUpView(view: View) {
        viewManager = LinearLayoutManager(context)
        mybillRecyclerViewAdapter.mValues = DummyContent.ITEMS
        view.list.apply {
            layoutManager = viewManager
            adapter = mybillRecyclerViewAdapter
            addItemDecoration(
                (DividerItemDecoration(
                    context!!,
                    DividerItemDecoration.VERTICAL
                ))
            )
        }

        colorDrawableBackground = ColorDrawable(Color.parseColor("#ff0000"))
        colorDrawableBackgroundEdit = ColorDrawable(Color.parseColor("#1b5df7"))
        deleteIcon = ContextCompat.getDrawable(context!!, R.drawable.ic_delete)!!
        editIcon = ContextCompat.getDrawable(context!!, R.drawable.ic_pencel)!!
        deleteIcon.setTint(Color.parseColor("#ffffff"))
        editIcon.setTint(Color.parseColor("#ffffff"))

        val itemTouchHelperCallback = object :
            ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT) {
            override fun onMove(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                viewHolder2: RecyclerView.ViewHolder
            ): Boolean {
                return false
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, swipeDirection: Int) {
                when (swipeDirection) {
                    ItemTouchHelper.RIGHT -> {
                        mybillRecyclerViewAdapter.notifyItemChanged(viewHolder.adapterPosition)
                        Log.e("Swipe", "Edit" + viewHolder.adapterPosition)
                        val dummy =
                            mybillRecyclerViewAdapter.mValues[viewHolder.adapterPosition].price.toString()
                        EditDialog(activity!!, object : EditDialog.EditDialogCallback {
                            override fun dismiss() {

                            }
                        }).show(dummy)
                    }
                    ItemTouchHelper.LEFT -> {
                        Log.e("Swipe", "Delete" + viewHolder.adapterPosition)
                        mybillRecyclerViewAdapter.notifyItemRemoved(viewHolder.adapterPosition)
                    }
                }
            }

            override fun onChildDraw(
                c: Canvas,
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                dX: Float,
                dY: Float,
                actionState: Int,
                isCurrentlyActive: Boolean
            ) {
                val itemView = viewHolder.itemView
                val iconMarginVertical =
                    (viewHolder.itemView.height - deleteIcon.intrinsicHeight) / 2

                if (dX > 0) {
                    colorDrawableBackgroundEdit.setBounds(
                        itemView.left,
                        itemView.top,
                        dX.toInt(),
                        itemView.bottom
                    )
                    editIcon.setBounds(
                        itemView.left + iconMarginVertical,
                        itemView.top + iconMarginVertical,
                        itemView.left + iconMarginVertical + editIcon.intrinsicWidth,
                        itemView.bottom - iconMarginVertical
                    )
                    colorDrawableBackgroundEdit.draw(c)
                } else {
                    colorDrawableBackground.setBounds(
                        itemView.right + dX.toInt(),
                        itemView.top,
                        itemView.right,
                        itemView.bottom
                    )
                    deleteIcon.setBounds(
                        itemView.right - iconMarginVertical - deleteIcon.intrinsicWidth,
                        itemView.top + iconMarginVertical,
                        itemView.right - iconMarginVertical,
                        itemView.bottom - iconMarginVertical
                    )
                    deleteIcon.level = 0
                    colorDrawableBackground.draw(c)
                }

                c.save()

                if (dX > 0) {
                    c.clipRect(itemView.left, itemView.top, dX.toInt(), itemView.bottom)
                    editIcon.draw(c)
                } else {
                    c.clipRect(
                        itemView.right + dX.toInt(),
                        itemView.top,
                        itemView.right,
                        itemView.bottom
                    )
                    deleteIcon.draw(c)
                }
                c.restore()

                super.onChildDraw(
                    c,
                    recyclerView,
                    viewHolder,
                    dX,
                    dY,
                    actionState,
                    isCurrentlyActive
                )
            }
        }

        val itemTouchHelper = ItemTouchHelper(itemTouchHelperCallback)
        itemTouchHelper.attachToRecyclerView(view.list)
    }

    private fun setIcon(id: Int,color: String):Drawable{
        val icon = ContextCompat.getDrawable(context!!, id)!!
        icon.setTint(Color.parseColor(color))
        return icon

    }

    interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onListFragmentInteraction(item: DummyItem?)
    }
}
