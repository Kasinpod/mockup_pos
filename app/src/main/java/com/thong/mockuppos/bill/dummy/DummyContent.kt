package com.thong.mockuppos.bill.dummy

import java.util.ArrayList
import java.util.HashMap

object DummyContent {

    val ITEMS: MutableList<DummyItem> = ArrayList()

    data class DummyItem(
        val id: Long,
        val name: String,
        val qty: Int,
        val price: Double = 0.0,
        val imageUrl: String
    )
}
