package com.thong.mockuppos.bill

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.core.view.isVisible
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.thong.mockuppos.R
import com.thong.mockuppos.utility.setShackAnimation
import kotlinx.android.synthetic.main.bottom_sheet_summary_retail_sale.view.*

class BottomSheetSummaryRetailSale : BottomSheetDialogFragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.bottom_sheet_summary_retail_sale, container, false)

        initView(view)
        onEvent(view)
        return view
    }

    private fun initView(view: View) {

        val arrList = arrayListOf<String>()
        arrList.add("บัตรสวัสดิการรัฐ")
        arrList.add("บัตรเครดิต")

        val mTopUpMoneyAdapter = PaymentTypeAdapter(this@BottomSheetSummaryRetailSale.context!!)
        view.recyclerViewPaymentType.layoutManager =
            LinearLayoutManager(context, RecyclerView.HORIZONTAL, true)
        mTopUpMoneyAdapter.itemList = arrList
        view.recyclerViewPaymentType.adapter = mTopUpMoneyAdapter
    }

    private fun onEvent(view: View) {
        view.buttonPayment.setOnClickListener {
            if (view.editTextCash.text.trim().isEmpty()) {
                view.linearViewCash.setShackAnimation(this.context!!)
                view.editTextCash.setText("")
                view.editTextCash.requestFocus()
            } else {
                this.dismiss()
            }
        }

        view.textViewPaymentSelect.setOnClickListener {
            if (view.recyclerViewPaymentType.isVisible) {
                view.recyclerViewPaymentType.visibility = View.GONE
                val animation: Animation = AnimationUtils.loadAnimation(
                    this.context!!,
                    R.anim.slide_to_left
                )
                view.recyclerViewPaymentType.animation = animation
            } else {
                view.recyclerViewPaymentType.visibility = View.VISIBLE
                val animation: Animation = AnimationUtils.loadAnimation(
                    this.context!!,
                    R.anim.slide_to_right
                )
                view.recyclerViewPaymentType.animation = animation
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NORMAL, R.style.DialogStyleTran)
    }
}