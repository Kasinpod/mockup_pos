package com.thong.mockuppos.bill

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import androidx.fragment.app.DialogFragment
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import android.view.View
import android.view.ViewGroup
import com.thong.mockuppos.R
import kotlinx.android.synthetic.main.bottom_sheet_update_qty.*
import kotlinx.android.synthetic.main.bottom_sheet_update_qty.view.*

class BottomSheetEditQtyItem : BottomSheetDialogFragment() {

    private var qty: Int = 1

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.bottom_sheet_update_qty, container, false)

        onEvent(view)
        return view
    }

    private fun onEvent(view: View) {
        view.buttonEditItem.setOnClickListener {
            dismiss()
        }

        view.buttonPlus.setOnClickListener {
            qty += 1
            editTextQty.setText("" + qty)
        }

        view.buttonDel.setOnClickListener {
            if (editTextQty.text.toString().trim().toInt() == 1) {
                qty = 1
                editTextQty.setText("1")
            } else {
                qty -= 1
                editTextQty.setText("" + qty)
            }
        }

        view.editTextQty.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                if(p0.toString().isEmpty()){
                    editTextQty.setText("1")
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }
        })
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NORMAL, R.style.DialogStyleTran)
    }
}