package com.thong.mockuppos.bill

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.thong.mockuppos.R

//ChangeLangAdapter
class PaymentTypeAdapter(private val mContext: Context) :
    RecyclerView.Adapter<PaymentTypeAdapter.ViewHolder>() {

    var itemList: ArrayList<String> = arrayListOf()

//    lateinit var recyclerView: RecyclerViewCallback.RecyclerViewCallBackListener
//
//    fun setOnSelectedItemListener(listener: RecyclerViewCallback.RecyclerViewCallBackListener){
//        recyclerView = listener
//    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.textViewPaymentType.text = itemList[position]
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.item_payment_type, parent, false)
        return ViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return itemList.size
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        internal var textViewPaymentType: TextView =
            itemView.findViewById(R.id.textViewPaymentType) as TextView
    }

}
