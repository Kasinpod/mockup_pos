package com.thong.mockuppos.bill

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.thong.mockuppos.R


import com.thong.mockuppos.bill.BillFragment.OnListFragmentInteractionListener
import com.thong.mockuppos.bill.dummy.DummyContent.DummyItem

import kotlinx.android.synthetic.main.fragment_bill.view.*

class MybillRecyclerViewAdapter(private val mContext: Context) :
    RecyclerView.Adapter<MybillRecyclerViewAdapter.ViewHolder>() {

    private val mOnClickListener: View.OnClickListener
    var mValues: List<DummyItem> = ArrayList()

    init {
        mOnClickListener = View.OnClickListener { v ->
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_bill, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = mValues[position]

        with(holder.mView) {
            tag = item
            setOnClickListener(mOnClickListener)
        }

        holder.textPrice.text = (item.qty * item.price).toString()
        holder.textQTY.text = "จำนวน  : ${item.qty}  รายการ"
        Glide.with(mContext)
            .load(item.imageUrl)
            .transform(CenterCrop(), RoundedCorners(30))
            .into(holder.image_icon)
    }

    override fun getItemCount(): Int = mValues.size

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        var image_icon = itemView.findViewById<ImageView>(R.id.image_icon)
        var textQTY = itemView.findViewById<TextView>(R.id.textQTY)
        var textPrice = itemView.findViewById<TextView>(R.id.textPrice)
    }
}
