package com.thong.mockuppos.bill

import android.app.Activity
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import com.thong.mockuppos.R


class EditDialog(var activity: Activity, var editDialogCallback: EditDialogCallback) {


    fun show(price: String
    ) {

        val builder = AlertDialog.Builder(activity)
        val viewGroup = activity.findViewById(R.id.content) as ViewGroup
        val dialogView = LayoutInflater.from(activity).inflate(R.layout.update_price_dialog, viewGroup, false)
        builder.setView(dialogView)
        val alertDialog = builder.create()
        alertDialog.setCanceledOnTouchOutside(false)
        val editText = dialogView.findViewById(R.id.priceUpdate) as EditText
        editText.setText(price)
        val alertConfirm = dialogView.findViewById(R.id.confirm) as Button
        alertDialog.show()

        alertConfirm.setOnClickListener {
            Log.e("Price is ", editText.text.toString())
            alertDialog.dismiss()
            editDialogCallback.dismiss()
        }
    }

    interface EditDialogCallback {
        fun dismiss()
    }
}