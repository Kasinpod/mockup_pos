package com.thong.mockuppos

import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import androidx.fragment.app.DialogFragment
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import android.widget.LinearLayout
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog

class BottomShoppingBoard : BottomSheetDialogFragment() {

    var root: View? = null
    private var listener: ModalCallback? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val bottomSheetView =
            inflater.inflate(R.layout.bottom_sheet_shopping_board, container, false)
        root = bottomSheetView
        return bottomSheetView
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        listener?.onModalHide()
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val bottomSheetDialog = super.onCreateDialog(savedInstanceState) as BottomSheetDialog
        bottomSheetDialog.setOnShowListener {
            val dialog = it as BottomSheetDialog
            val bottomSheet =
                dialog.findViewById<View>(com.google.android.material.R.id.design_bottom_sheet)
            val bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet)
            bottomSheetBehavior.setBottomSheetCallback(object :
                BottomSheetBehavior.BottomSheetCallback() {
                override fun onSlide(bottomSheet: View, slideOffset: Float) {
                    Log.e("bottomSheet", "onSlide$bottomSheet")
                    Log.e("slideOffset", "onSlide$slideOffset")
                }

                override fun onStateChanged(bottomSheet: View, newState: Int) {
                    Log.e("bottomSheet", "onStateChanged$bottomSheet")
                    Log.e("slideOffset", "onStateChanged$newState")

                    when (newState) {
                        BottomSheetBehavior.STATE_EXPANDED -> {
                            listener?.onModalShow()
                        }
                        BottomSheetBehavior.STATE_HIDDEN -> {
                            dismiss()
                            listener?.onModalHide()
                        }
                    }
                }
            })
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
        }
        return bottomSheetDialog
    }

    override fun onStart() {
        super.onStart()
        dialog?.also {
            val bottomSheet = dialog?.findViewById<View>(R.id.design_bottom_sheet)
            bottomSheet?.layoutParams?.height = ViewGroup.LayoutParams.MATCH_PARENT
            val behavior = BottomSheetBehavior.from<View>(bottomSheet)
            val layout =
                root?.findViewById(R.id.rootLayout) as LinearLayout //rootLayout is root of your fragment layout.
            layout.viewTreeObserver?.addOnGlobalLayoutListener(object :
                ViewTreeObserver.OnGlobalLayoutListener {
                override fun onGlobalLayout() {
                    try {
                        layout.viewTreeObserver.removeGlobalOnLayoutListener(this)
                        behavior.peekHeight = layout.height
                        view?.requestLayout()
                    } catch (e: Exception) {
                    }
                }
            })
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NORMAL, R.style.DialogStyle)
    }

    interface ModalCallback{
        fun onModalHide()
        fun onModalShow()
    }

    fun setOnModalEventListener(listener: ModalCallback){
        this.listener = listener
    }
}