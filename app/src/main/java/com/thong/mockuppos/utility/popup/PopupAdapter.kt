package com.thong.mockuppos.utility.popup

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.thong.mockuppos.R

class PopupAdapter : RecyclerView.Adapter<PopupAdapter.ViewHolder>() {

    lateinit var callBack: (Int) -> Unit

    fun setOnClickListener(callBack: (position: Int) -> Unit) {
        this.callBack = callBack
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.item_popup_adapter, parent, false)
        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.textTitle.text = "Show $position"
        holder.itemView.setOnClickListener {
            callBack.invoke(position)
        }

    }


    override fun getItemCount(): Int {
        return 20
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var textTitle: TextView = view.findViewById(R.id.tv_item)

    }

}