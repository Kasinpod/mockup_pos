package com.thong.mockuppos.utility.swipe

interface SwipeControllerActions {
    fun onLeftClicked(position: Int) {}

    fun onRightClicked(position: Int) {}
}