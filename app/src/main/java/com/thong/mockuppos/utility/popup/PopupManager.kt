package com.thong.mockuppos.utility.popup

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.widget.PopupWindow
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.thong.mockuppos.R

class PopupManager (context: Context) : PopupWindow() {

    private var mContext: Context = context
//    private var mData: List<String> = list
    private lateinit var popupWinDow: PopupWindow
    private var recyclerView: RecyclerView? = null

    lateinit var callBack: (Int) -> Unit

    fun setOnCallBackListener(callBack: (position: Int) -> Unit) {
        this.callBack = callBack
    }

    fun showPopupWindow(anchor: View, width: Int, height: Int){
        val contentView = LayoutInflater.from(mContext).inflate(R.layout.popup_window,null)
        popupWinDow = PopupWindow(contentView,width,height,true)
        popupWinDow.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        popupWinDow.isOutsideTouchable = true

        recyclerView = contentView.findViewById(R.id.recyclerview_popup)
        val popupAdapter = PopupAdapter()

        recyclerView?.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(mContext)
            adapter = popupAdapter
        }

        popupAdapter.setOnClickListener {
            Toast.makeText(mContext, "position $it", Toast.LENGTH_SHORT).show()
            callBack.invoke(it)
            destroyPopWindow()
        }

        popupWinDow.showAsDropDown(anchor,0,0)


    }

    private fun destroyPopWindow() {
        Handler().postDelayed({
            popupWinDow.dismiss()
        },400)
    }
}