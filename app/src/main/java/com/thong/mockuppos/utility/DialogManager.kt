package com.thong.mockuppos.utility

import android.app.Activity
import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.media.Image
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.Window
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import com.thong.mockuppos.R


class DialogManager(var activity: Activity, var dialogCallback: DialogManagerCallback) {

    fun show() {

        var alertdialog = Dialog(activity)
        alertdialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertdialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        alertdialog.setContentView(R.layout.dialog_fast_sale)
        alertdialog.setCancelable(false)

        val imageViewButtonClose = alertdialog.findViewById(R.id.imageViewButtonClose) as ImageView
        val editTextProductName = alertdialog.findViewById(R.id.editTextProductName) as EditText
        val editTextProductPrice = alertdialog.findViewById(R.id.editTextProductPrice) as EditText
        val buttonSale = alertdialog.findViewById(R.id.buttonSale) as TextView

        alertdialog.show()

        buttonSale.setOnClickListener {
            if (editTextProductPrice.text.toString().trim().isEmpty()) {
                editTextProductPrice.setShackAnimation(activity)
            } else {
                alertdialog.dismiss()
            }
        }

        imageViewButtonClose.setOnClickListener {
            alertdialog.dismiss()
        }
    }

    interface DialogManagerCallback {
        fun confirm(data: Any)
    }
}