package com.thong.mockuppos.shopping


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.thong.mockuppos.R
import com.thong.mockuppos.hotkey.Hotkey
import kotlinx.android.synthetic.main.fragment_add_hotkey.view.*

class ShoppingFragment : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view= inflater.inflate(R.layout.fragment_shopping, container, false)
        return view
    }
}
