package com.thong.mockuppos.scanner


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop

import com.thong.mockuppos.R
import kotlinx.android.synthetic.main.fragment_result_scaner.view.*

class ResultScanerFragment : Fragment() {

    var resultScanerFragmentCallback: ResultScanerFragmentCallback? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_result_scaner, container, false)
        initView(view)
        onEvent(view)
        return view
    }

    private fun initView(view: View) {
        Glide.with(context!!)
            .load("https://image.makewebeasy.net/makeweb/0/cVLsH6ZXs/DefaultData/8850987101021_1.jpg")
            .transform(CenterCrop())
            .into(view.imageView)

        view.textViewBarcode.text = arguments?.getString("barcode") ?: "-"
        view.textViewQTY.text = arguments?.getString("qty") ?: "1"
        view.textViewPrice.text = "${(arguments?.getString("qty")?.toInt() ?: 1) * 6}.00"

        if (arguments?.getBoolean("hideBarcode", false)!!) {
            view.textViewBarcode.visibility = View.GONE
        }
    }

    private fun onEvent(view: View) {
        view.button_del_item.setOnClickListener {
            activity!!.supportFragmentManager.beginTransaction().remove(this).commit()
        }

        view.cardResultItem.setOnClickListener {
            val bottomSheetEditQtyPriceItem = BottomSheetEditQtyPriceItem()
            bottomSheetEditQtyPriceItem.show(
                activity!!.supportFragmentManager,
                BottomSheetEditQtyPriceItem::class.java.simpleName
            )

            bottomSheetEditQtyPriceItem.bottomSheetEditQtyPriceItemCallback = object :
                BottomSheetEditQtyPriceItem.BottomSheetEditQtyPriceItemCallback {
                override fun dismissBottomSheet() {
                    resultScanerFragmentCallback?.dismissBottomSheet()
                }

                override fun openBottomSheet() {

                }
            }
            resultScanerFragmentCallback?.openBottomSheet()
        }
    }

    interface ResultScanerFragmentCallback {
        fun openBottomSheet()
        fun dismissBottomSheet()
    }
}
