package com.thong.mockuppos.scanner

import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.zxing.Result

import com.thong.mockuppos.R
import com.thong.mockuppos.utility.DialogManager
import kotlinx.android.synthetic.main.fragment_scanner.*
import kotlinx.android.synthetic.main.fragment_scanner.view.*
import me.dm7.barcodescanner.zxing.ZXingScannerView

class ScannerFragment : Fragment(), ZXingScannerView.ResultHandler {

    private var qty: Int = 1
    private var rootView: View? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_scanner, container, false)
        rootView = view
        onEvent(view)
        return view
    }

    private fun onEvent(view: View) {
        view.buttonPlus.setOnClickListener {
            qty += 1
            editTextQty.setText("" + qty)
        }

        view.buttonDel.setOnClickListener {
            if (editTextQty.text.toString().trim().toInt() == 1) {
                qty = 1
                editTextQty.setText("1")
            } else {
                qty -= 1
                editTextQty.setText("" + qty)
            }
        }

        view.editTextQty.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                if (p0.toString().isEmpty()) {
                    editTextQty.setText("1")
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }
        })
    }

    override fun onResume() {
        super.onResume()
        if (viewScan != null) {
            Handler().postDelayed({
                viewScan.setResultHandler(this)
                viewScan.startCamera()
            }, 350)
        }
    }

    override fun onPause() {
        super.onPause()
        if (viewScan != null) {
            viewScan.stopCamera()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (viewScan != null) {
            viewScan.stopCamera()
        }
    }

    fun startCamera() {
        if (viewScan != null) {
            viewScan.startCamera()
        }
    }

    fun stopCamera(){
        if (viewScan != null) {
            viewScan.stopCamera()
        }
    }

    override fun handleResult(rawResult: Result?) {
        viewScan.resumeCameraPreview(this)
        Log.e("ResultScan", rawResult.toString())
        if (rawResult.toString() == "8851102003091") {
//            onPause()
            DialogManager(activity!!, object : DialogManager.DialogManagerCallback {
                override fun confirm(data: Any) {
//                    onResume()
                }
            }).show()
        } else {
            val fragment = ResultScanerFragment()
            val bundle = Bundle()
            bundle.putString("barcode", rawResult.toString())
            bundle.putBoolean("hideBarcode", false)
            bundle.putString("qty", rootView!!.editTextQty.text.toString())
            fragment.arguments = bundle
            val manager = this.activity!!.supportFragmentManager
            val transaction = manager.beginTransaction()
            transaction.replace(frame_scanner_result.id, fragment)
            transaction.commit()
        }
        rootView!!.editTextQty.setText("1")
    }
}
