package com.thong.mockuppos.scanner

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import androidx.fragment.app.DialogFragment
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import android.view.View
import android.view.ViewGroup
import com.thong.mockuppos.R
import kotlinx.android.synthetic.main.bottom_sheet_edit_item_qty_price.*
import kotlinx.android.synthetic.main.bottom_sheet_edit_item_qty_price.view.*

class BottomSheetEditQtyPriceItem : BottomSheetDialogFragment() {

    private var qty: Int = 1
    private var price: Double = 1.00
    var bottomSheetEditQtyPriceItemCallback: BottomSheetEditQtyPriceItemCallback? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.bottom_sheet_edit_item_qty_price, container, false)

        onEvent(view)
        return view
    }

    private fun onEvent(view: View) {
        view.buttonEditItem.setOnClickListener {
            dismiss()
        }

        view.buttonPlus.setOnClickListener {
            qty += 1
            editTextQty.setText("" + qty)
        }

        view.buttonDel.setOnClickListener {
            if (editTextQty.text.toString().trim().toInt() == 1) {
                qty = 1
                editTextQty.setText("1")
            } else {
                qty -= 1
                editTextQty.setText("" + qty)
            }
        }

        view.editTextQty.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                if(p0.toString().isEmpty()){
                    editTextQty.setText("1")
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }
        })

        view.buttonPricePlus.setOnClickListener {
            price += 1.00
            editTextPrice.setText("" + price)
        }

        view.buttonPriceDel.setOnClickListener {
            if (editTextPrice.text.toString().trim().toDouble() == 1.00) {
                price = 1.00
                editTextPrice.setText("1.00")
            } else {
                price -= 1.00
                editTextPrice.setText("" + price)
            }
        }

        view.editTextPrice.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                if(p0.toString().isEmpty()){
                    editTextPrice.setText("1.00")
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }
        })
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NORMAL, R.style.DialogStyleTran)
    }

    override fun dismiss() {
        super.dismiss()
        bottomSheetEditQtyPriceItemCallback?.dismissBottomSheet()
    }

    override fun onDestroy() {
        super.onDestroy()
        bottomSheetEditQtyPriceItemCallback?.dismissBottomSheet()
    }


    interface BottomSheetEditQtyPriceItemCallback {
        fun openBottomSheet()
        fun dismissBottomSheet()
    }
}