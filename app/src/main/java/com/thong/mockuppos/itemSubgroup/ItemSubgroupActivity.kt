package com.thong.mockuppos.itemSubgroup

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.thong.mockuppos.R
import com.thong.mockuppos.hotkey.Hotkey
import com.thong.mockuppos.hotkeySelected.HotkeySelectAdapter
import kotlinx.android.synthetic.main.activity_item_subgroup.*


class ItemSubgroupActivity : AppCompatActivity(),HotkeySelectAdapter.HotkeySelectAdapterCallback {

    private lateinit var hotkeySelectAdapter: HotkeySelectAdapter
    private var hotkey: Hotkey? = null
    private var lists: ArrayList<Hotkey> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_item_subgroup)
        hotkeySelectAdapter = HotkeySelectAdapter(this)
        hotkeySelectAdapter.hotkeySelectAdapterCallback = this

        setData()

        onEvent()

        setRecyclerView()
    }

    private fun setData() {
        if (intent == null) return
        val bundle = intent.extras
        hotkey = bundle?.getSerializable("sub_group") as Hotkey?
        textTitleSubGroup.text = hotkey?.title
        lists.add(hotkey!!)
    }

    private fun onEvent() {
        imageViewCloseSubgroup.setOnClickListener {
            finish()
        }
    }

    private fun setRecyclerView() {
        itemSubgroupRecyclerView.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(this@ItemSubgroupActivity)
            hotkeySelectAdapter.mValues = lists
            adapter = hotkeySelectAdapter
        }
    }

    override fun itemSelected(`objects`: Any) {
        super.itemSelected(`objects`)
        val model = `objects` as Hotkey
        val bundle = Bundle()
        bundle.putSerializable("data",model)
        val intent = Intent()
        intent.putExtras(bundle)
        setResult(Activity.RESULT_OK, intent)
        finish()
    }
}

