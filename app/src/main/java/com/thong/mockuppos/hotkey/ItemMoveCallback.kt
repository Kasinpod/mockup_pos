package com.thong.mockuppos.hotkey

import android.graphics.Canvas
import android.util.Log
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView

class ItemMoveCallback(val hotKeyAdapter: HotKeyAdapter) :
    ItemTouchHelper.Callback() {
    override fun getMovementFlags(
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder
    ): Int {
        val dragFlags =
            ItemTouchHelper.UP or ItemTouchHelper.DOWN or ItemTouchHelper.START or ItemTouchHelper.END
        return makeMovementFlags(dragFlags, 0)
    }

    override fun onMove(
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        target: RecyclerView.ViewHolder
    ): Boolean {
        val isState =
            (hotKeyAdapter.items[viewHolder.adapterPosition].type == 0 && hotKeyAdapter.items[target.adapterPosition].type == 0)
        if (isState) {
            hotKeyAdapter.onRowMoved(viewHolder.adapterPosition, target.adapterPosition)
        }
        return isState
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
    }

    override fun isItemViewSwipeEnabled(): Boolean {
        return false
    }

    override fun isLongPressDragEnabled(): Boolean {
        return true
    }

    override fun onChildDraw(
        c: Canvas,
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        dX: Float,
        dY: Float,
        actionState: Int,
        isCurrentlyActive: Boolean
    ) {
        if (isCurrentlyActive) {
            hotKeyAdapter.onDragRemove(viewHolder, dX, dY)
        } else if (!isCurrentlyActive && HotKeyFragment.isRemove && viewHolder is HotKeyAdapter.ViewHolder) {
            Log.e("Drag is ", "Remove")
            hotKeyAdapter.onRowClear(viewHolder)
        }
        super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
    }

    override fun clearView(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder) {
        if (viewHolder is HotKeyAdapter.ViewHolder) {
            hotKeyAdapter.onRowClear(viewHolder)
        }
    }

    override fun onSelectedChanged(viewHolder: RecyclerView.ViewHolder?, actionState: Int) {
        if (actionState != ItemTouchHelper.ACTION_STATE_IDLE) {
            if (viewHolder is HotKeyAdapter.ViewHolder) {
                hotKeyAdapter.onRowSelected(viewHolder)
            }
        }
    }

    interface ItemTouchHelperHotkey {
        fun onRowMoved(fromPosition: Int, toPosition: Int)
        fun onRowSelected(viewHolder: HotKeyAdapter.ViewHolder)
        fun onRowClear(viewHolder: HotKeyAdapter.ViewHolder)
        fun onDragRemove(viewHolder: RecyclerView.ViewHolder, dX: Float, dY: Float)
    }
}