package com.thong.mockuppos.hotkey


import android.os.Bundle
import android.os.Handler
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.GridLayoutManager
import com.thong.mockuppos.R
import com.thong.mockuppos.utility.popup.PopupManager
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.thong.mockuppos.bill.dummy.DummyContent
import com.thong.mockuppos.hotkeySelected.BottomHotkeySelect
import com.thong.mockuppos.scanner.ResultScanerFragment
import kotlinx.android.synthetic.main.fragment_hot_key.*
import kotlinx.android.synthetic.main.fragment_hot_key.view.*
import java.util.*
import kotlin.collections.ArrayList


class HotKeyFragment : Fragment(), BottomHotkeySelect.BottomHotkeySelectCallback,
    HotKeyAdapter.HotKeyAdapterCallback {

    private lateinit var hotKeyAdapter: HotKeyAdapter
    private var popupManager: PopupManager? = null
    private val handler = Handler()

    companion object {
        var isRemove = false
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_hot_key, container, false)
        hotKeyAdapter = HotKeyAdapter(context!!)
        hotKeyAdapter.hotKeyAdapterCallback = this
        setRecyclerView(view)
        onEvent(view)
        return view
    }

    private fun setRecyclerView(view: View) {
        view.removeView.setColorFilter(ContextCompat.getColor(context!!, R.color.colorGray))
        val callback = ItemMoveCallback(hotKeyAdapter)
        val touchHelper = ItemTouchHelper(callback)
        touchHelper.attachToRecyclerView(view.hotkey_recyclerView)
        view.hotkey_recyclerView.apply {
            setHasFixedSize(true)
            layoutManager = GridLayoutManager(context, 3)
            hotKeyAdapter.items = setData()
            adapter = hotKeyAdapter
        }
    }

    private fun onEvent(view: View) {
        hotKeyAdapter.setOnClickRecyclerViewListener { position, v ->
            if (hotKeyAdapter.items[position].type == 1) {
                openBottomSheetSelectHotkey()
            } else {
                val item = hotKeyAdapter.items[position]
                DummyContent.ITEMS.add(
                    DummyContent.DummyItem(
                        System.currentTimeMillis(),
                        item.title,
                        1,
                        item.price,
                        item.img_url
                    )
                )
                val fragment = ResultScanerFragment()
                val bundle = Bundle()
                bundle.putBoolean("hideBarcode", true)
                bundle.putString("qty", "1")
                fragment.arguments = bundle
                val manager = this.activity!!.supportFragmentManager
                val transaction = manager.beginTransaction()
                transaction.replace(view.frame_add.id, fragment)
                transaction.commit()

                val animation: Animation = AnimationUtils.loadAnimation(
                    context,
                    R.anim.slide_to_top
                )
                view.frame_add.animation = animation
                view.frame_add.visibility = View.VISIBLE
                handler.removeCallbacks(null)
                handler.postDelayed({
                    val animation: Animation = AnimationUtils.loadAnimation(
                        context,
                        R.anim.slide_to_bottom
                    )
                    view.frame_add.animation = animation
                    view.frame_add.visibility = View.GONE
                }, 2000)

                fragment.resultScanerFragmentCallback = object : ResultScanerFragment.ResultScanerFragmentCallback {
                    override fun openBottomSheet() {
                        handler.removeCallbacks(null)
                    }

                    override fun dismissBottomSheet() {
                        view.frame_add.visibility = View.VISIBLE
                        handler.removeCallbacks(null)
                        handler.postDelayed({
                            val animation: Animation = AnimationUtils.loadAnimation(
                                context,
                                R.anim.slide_to_bottom
                            )
                            view.frame_add.animation = animation
                            view.frame_add.visibility = View.GONE
                        }, 2000)
                    }

                }
            }
        }

    }

    private fun presentPopup(position: Int, itemView: View) {
        popupManager = PopupManager(itemView.context)
        popupManager!!.showPopupWindow(itemView, (itemView.width * 1.6).toInt(), 400)
        popupManager!!.setOnCallBackListener {
        }

    }

    private fun openBottomSheetSelectHotkey() {
        val selectHotkey = BottomHotkeySelect()
        selectHotkey.show(
            activity!!.supportFragmentManager,
            BottomHotkeySelect::class.java.simpleName
        )
        selectHotkey.bottomHotkeySelectCallback = this
    }

    override fun itemSelected(`object`: Any) {
        super.itemSelected(`object`)
        val item = `object` as Hotkey
        val index = hotKeyAdapter.items.indexOfFirst { it.type == 1 }
        if (item.type == 0) {
            val hotkey = Hotkey()
            hotkey.title = item.title
            hotkey.img_url = item.img_url
            hotKeyAdapter.items.add(index, hotkey)
            hotKeyAdapter.notifyDataSetChanged()
        } else {
            AddHotkeyDialog(activity!!, object : AddHotkeyDialog.AddHotkeyDialogCallback {
                override fun confirm(data: Hotkey) {
                    hotKeyAdapter.items.add(index, data)
                    hotKeyAdapter.notifyDataSetChanged()
                }
            }).show()
        }
    }

    override fun onRowMoved(viewHolder: HotKeyAdapter.ViewHolder) {
        layoutRemove.visibility = View.VISIBLE
    }

    override fun onRowClear(viewHolder: HotKeyAdapter.ViewHolder) {
        layoutRemove.visibility = View.GONE
        if (isRemove && hotKeyAdapter.items[viewHolder.adapterPosition].type != 1) {
            hotkey_recyclerView.removeViewAt(viewHolder.adapterPosition)
            hotKeyAdapter.items.removeAt(viewHolder.adapterPosition)
            hotKeyAdapter.notifyDataSetChanged()
            isRemove = false
        }
    }

    override fun onDragRemove(viewHolder: RecyclerView.ViewHolder, dX: Float, dY: Float) {
        isRemove = isViewOverlapping(
            hotkey_recyclerView.getChildAt(viewHolder.adapterPosition),
            removeView
        )
        if (isRemove) {
            removeView.setColorFilter(ContextCompat.getColor(context!!, R.color.colorWhite))
            layoutRemove.background =
                ContextCompat.getDrawable(context!!, R.drawable.shap_bg_remove)
        } else {
            removeView.setColorFilter(ContextCompat.getColor(context!!, R.color.colorGray))
            layoutRemove.background = null
        }
    }

    private fun isViewOverlapping(firstView: View, secondView: View): Boolean {
        val firstPosition = IntArray(2)
        val secondPosition = IntArray(2)
        firstView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED)
        firstView.getLocationOnScreen(firstPosition)
        secondView.getLocationOnScreen(secondPosition)
        val r = firstView.measuredHeight + firstPosition[1]
        val l = secondPosition[1]
        return r >= l && r != 0 && l != 0
    }

    private fun setData(): ArrayList<Hotkey> {
        val hotkeys: ArrayList<Hotkey> = ArrayList()
        for (i in 0..4) {
            val hotkey = Hotkey()
            if (i == 4) {
                hotkey.title = "เพิ่ม"
                hotkey.type = 1
            } else {
                when (i) {
                    0 -> {
                        hotkey.title = "อาหารแปรรูป"
                        hotkey.img_url =
                            "https://www.technologychaoban.com/wp-content/uploads/2017/04/IMG_4866.jpg"
                        hotkey.price = 100.0
                    }
                    1 -> {
                        hotkey.title = "สินค้าจักรสาน"
                        hotkey.img_url =
                            "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRIH4VY05zqXoe2egQbMu8tChkbQwN_v1-A_lkTeuIoIEveGh6kQA&s"
                        hotkey.price = 200.0
                    }
                    2 -> {
                        hotkey.title = "สินค้าทางการเกษตร"
                        hotkey.img_url =
                            "https://image.bangkokbiznews.com/kt/media/image/news/2019/11/20/855363/750x422_855363_1574258063.JPG"
                        hotkey.price = 300.0
                    }
                    3 -> {
                        hotkey.title = "ข้าว"
                        hotkey.img_url =
                            "https://www.pumpanya.com/assets/upload/images/2019/05/ck20190516115917CvOlBHPBFC.jpg"
                        hotkey.price = 150.0
                    }
                }
            }
            hotkeys.add(hotkey)
        }
        return hotkeys
    }
}
