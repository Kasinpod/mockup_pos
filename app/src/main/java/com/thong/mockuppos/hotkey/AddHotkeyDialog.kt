package com.thong.mockuppos.hotkey

import android.app.Activity
import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.media.Image
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.Window
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import com.thong.mockuppos.R


class AddHotkeyDialog(var activity: Activity, var editDialogCallback: AddHotkeyDialogCallback) {

    fun show() {

//        val builder = AlertDialog.Builder(activity)
//        val viewGroup = activity.findViewById(R.id.content) as ViewGroup
//        val dialogView = LayoutInflater.from(activity).inflate(R.layout.add_hotkey_dialog, viewGroup, false)
//        builder.setView(dialogView)


        var alertdialog = Dialog(activity)
        alertdialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertdialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        alertdialog.setContentView(R.layout.add_hotkey_dialog)
        alertdialog.setCancelable(false)

        val takePhoto = alertdialog.findViewById(R.id.takePhoto) as ImageView
        val productName = alertdialog.findViewById(R.id.productName) as EditText
        val ProductPrice = alertdialog.findViewById(R.id.productPrice) as EditText
        val confirm = alertdialog.findViewById(R.id.confirm) as TextView
        val cancel = alertdialog.findViewById(R.id.cancel) as ImageView



        alertdialog.show()

        takePhoto.setColorFilter(ContextCompat.getColor(activity, R.color.colorLight))

        confirm.setOnClickListener {
            alertdialog.dismiss()
            val hotkey = Hotkey()
            hotkey.type = 0
            hotkey.title = productName.text.toString()
            hotkey.img_url = "https://www.cowgirlcontractcleaning.com/wp-content/uploads/sites/360/2018/05/placeholder-img-4.jpg"
            editDialogCallback.confirm(hotkey)
        }

        cancel.setOnClickListener {
            alertdialog.dismiss()
        }
    }

    interface AddHotkeyDialogCallback {
        fun confirm(data: Hotkey)
    }
}