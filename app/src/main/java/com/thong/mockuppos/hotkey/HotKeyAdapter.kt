package com.thong.mockuppos.hotkey

import android.content.Context
import android.content.res.Resources
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.view.setPadding
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.thong.mockuppos.R
import java.io.Serializable
import java.util.*
import kotlin.collections.ArrayList


class HotKeyAdapter(val context: Context) : RecyclerView.Adapter<HotKeyAdapter.ViewHolder>(),
    ItemMoveCallback.ItemTouchHelperHotkey {

    lateinit var callBack: (Int, View) -> Unit
    lateinit var hotKeyAdapterCallback: HotKeyAdapterCallback
    var items: ArrayList<Hotkey> = ArrayList()

    fun setOnClickRecyclerViewListener(callBack: (position: Int, view: View) -> Unit) {
        this.callBack = callBack
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.item_hotkey_adapter, parent, false)
        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val hotkey = items[position]
        val width: Int = Resources.getSystem().displayMetrics.widthPixels
        val params = holder.layout.layoutParams
        params.width = width / 3
        params.height = width / 3

        holder.textTitle.text = hotkey.title
        when (hotkey.type) {
            1 -> {
                holder.iconImge.setImageDrawable(
                    ContextCompat.getDrawable(
                        context,
                        R.drawable.ic_add
                    )
                )

                holder.iconImge.setColorFilter(
                    ContextCompat.getColor(
                        context,
                        R.color.colorPrimary
                    ), android.graphics.PorterDuff.Mode.SRC_IN
                )
                holder.iconImge.setPadding(90)
            }
            else -> {
                Glide.with(context)
                    .load(hotkey.img_url)
                    .transform(CenterCrop())
                    .into(holder.iconImge)
            }
        }
        holder.itemView.setOnClickListener {
            callBack.invoke(position, it)
        }
    }


    override fun getItemCount(): Int {
        return items.size
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var iconImge: ImageView = view.findViewById(R.id.icon_image_hotkey)
        var textTitle: TextView = view.findViewById(R.id.text_title_hotkey)
        var layout: LinearLayout = view.findViewById(R.id.layout_item_hotkey)
    }


    override fun onRowMoved(fromPosition: Int, toPosition: Int) {
        if (fromPosition < toPosition) {
            for (i in fromPosition until toPosition) {
                Collections.swap(items, i, i + 1)
            }
        } else {
            for (i in fromPosition downTo toPosition + 1) {
                Collections.swap(items, i, i - 1)
            }
        }
        notifyItemMoved(fromPosition, toPosition)
    }

    override fun onRowSelected(viewHolder: ViewHolder) {
        hotKeyAdapterCallback.onRowMoved(viewHolder)
    }

    override fun onRowClear(viewHolder: ViewHolder) {
        hotKeyAdapterCallback.onRowClear(viewHolder)
    }

    override fun onDragRemove(viewHolder: RecyclerView.ViewHolder, dX: Float, dY: Float) {
        hotKeyAdapterCallback.onDragRemove(viewHolder, dX, dY)
    }

    interface HotKeyAdapterCallback {
        fun onRowMoved(viewHolder: ViewHolder)
        fun onRowClear(viewHolder: ViewHolder)
        fun onDragRemove(viewHolder: RecyclerView.ViewHolder, dX: Float, dY: Float)
    }
}


/** type: 0 = other, 1 = add **/
class Hotkey : Serializable {
    var type: Int = 0
    var title: String = ""
    var img_url: String = ""
    var price: Double = 0.0
}