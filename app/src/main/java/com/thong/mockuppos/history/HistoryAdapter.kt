package com.thong.mockuppos.history

import android.content.Context
import android.graphics.Typeface
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.thong.mockuppos.R
import org.zakariya.stickyheaders.SectioningAdapter


class HistoryAdapter(val context: Context) : SectioningAdapter() {

    var sectionDummys: ArrayList<SectionDummy> = ArrayList()
    private val USE_DEBUG_APPEARANCE = false

    override fun onCreateItemViewHolder(parent: ViewGroup?, itemUserType: Int): ItemViewHolder {
        val inflater = LayoutInflater.from(parent?.context)
        val v = inflater.inflate(R.layout.item_bill_history, parent, false)
        return ItemViewHolder(v)
    }

    override fun onCreateHeaderViewHolder(
        parent: ViewGroup?,
        headerUserType: Int
    ): HeaderViewHolder {
        val inflater = LayoutInflater.from(parent?.context)
        val v = inflater.inflate(R.layout.item_footer, parent, false)
        return HeaderViewHolder(v)
    }

    override fun onBindHeaderViewHolder(
        viewHolder: HeaderViewHolder?,
        sectionIndex: Int,
        headerUserType: Int
    ) {

    }

    override fun onBindItemViewHolder(
        viewHolder: SectioningAdapter.ItemViewHolder?,
        sectionIndex: Int,
        itemIndex: Int,
        itemUserType: Int
    ) {
        val view = viewHolder as ItemViewHolder
        val item = sectionDummys[sectionIndex].items[itemIndex] as ItemDummy
        view.title.text = item.billNumber
        view.textPrice.text = item.price.toString()
        view.textDescription.text = "สินค้า ${item.count} รายการ"
        if (itemIndex == 0) {
            view.dateLayout.visibility = View.VISIBLE
            view.textMonth.text = sectionDummys[sectionIndex].month
            view.textDay.text = sectionDummys[sectionIndex].day
            view.textYear.text = sectionDummys[sectionIndex].year
        } else {
            view.dateLayout.visibility = View.INVISIBLE
        }

        if (sectionIndex == 0){
            view.textDay.setTextColor(ContextCompat.getColor(context,R.color.colorPrimary))
            view.textDay.setTypeface(null,Typeface.BOLD)
        }else{
            view.textDay.setTextColor(ContextCompat.getColor(context,R.color.colorPrimaryDark))
            view.textDay.setTypeface(null,Typeface.NORMAL)
        }
    }

    inner class ItemViewHolder(itemView: View) : SectioningAdapter.ItemViewHolder(itemView) {
        val title = itemView.findViewById(R.id.textTitle) as TextView
        val textDescription = itemView.findViewById(R.id.textDescription) as TextView
        val dateLayout = itemView.findViewById(R.id.dateLayout) as LinearLayout
        val textPrice = itemView.findViewById(R.id.textPrice) as TextView
        val textMonth = itemView.findViewById(R.id.textMonth) as TextView
        val textDay = itemView.findViewById(R.id.textDay) as TextView
        val textYear = itemView.findViewById(R.id.textYear) as TextView
    }

    override fun doesSectionHaveHeader(sectionIndex: Int): Boolean {
        return true
    }

    override fun getNumberOfSections(): Int {
        return sectionDummys.size
    }

    override fun getNumberOfItemsInSection(sectionIndex: Int): Int {
        return sectionDummys[sectionIndex].items.size
    }

    override fun onBindGhostHeaderViewHolder(
        viewHolder: GhostHeaderViewHolder,
        sectionIndex: Int
    ) {
        if (USE_DEBUG_APPEARANCE) {
            viewHolder.itemView.setBackgroundColor(-0x666601)
        }
    }
}


class SectionDummy {
    var day = ""
    var month = ""
    var year = ""
    val items: ArrayList<ItemDummy> = ArrayList()
}


class ItemDummy {
    var billNumber = ""
    var count = 0
    var price = 0.0
}