package com.thong.mockuppos.history

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.thong.mockuppos.R
import kotlinx.android.synthetic.main.fragment_hot_key.view.*
import org.zakariya.stickyheaders.StickyHeaderLayoutManager


class HistoryFragment : Fragment() {

    private lateinit var historyAdapter: HistoryAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_hot_key, container, false)
        historyAdapter = HistoryAdapter(context!!)
        setRecyclerView(view)
        return view
    }

    private fun setRecyclerView(view: View) {
        view.hotkey_recyclerView.apply {
            layoutManager = StickyHeaderLayoutManager()
            historyAdapter.sectionDummys = setData()
            adapter = historyAdapter
        }
    }

    private fun setData(): ArrayList<SectionDummy> {
        val sections: ArrayList<SectionDummy> = ArrayList()

        for (i in 1..6) {
            val section = SectionDummy()
            val items: ArrayList<ItemDummy> = ArrayList()
            section.day = "${7-i}"
            section.month = "ม.ค"
            section.year = "2020"
            for (j in i..6) {
                val itemDummy = ItemDummy()
                itemDummy.billNumber = "${System.currentTimeMillis()}"
                itemDummy.price = ((i+1) * 10).toDouble()
                itemDummy.count = i + 2
                items.add(itemDummy)
            }
            section.items.addAll(items)
            sections.add(section)
        }

        return sections
    }
}
