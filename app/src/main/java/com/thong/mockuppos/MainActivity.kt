package com.thong.mockuppos

import android.annotation.SuppressLint
import android.graphics.Point
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.DragEvent
import android.view.MotionEvent
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.thong.mockuppos.bill.BillFragment
import com.thong.mockuppos.hotkey.HotKeyFragment
import kotlinx.android.synthetic.main.activity_main.*
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.LinearLayout
import androidx.constraintlayout.widget.ConstraintLayout
import com.iammert.library.AnimatedTabLayout
import com.thong.mockuppos.bill.dummy.DummyContent
import com.thong.mockuppos.history.HistoryFragment
import com.thong.mockuppos.scanner.ScannerFragment
import kotlin.collections.ArrayList
import kotlin.math.abs
import android.widget.AbsoluteLayout
import com.thong.mockuppos.shopping.ShoppingFragment


class MainActivity : AppCompatActivity() {

    private var newX = 0f
    private var newY = 0f
    private var oldX = 0f
    private var oldY = 0f
    private var dX = 0f
    private var dY = 0f

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        onEvent()
        /*openBottomSheetShoppingBoard()*/
        setDummyData()
        setView()
    }

    private fun setView() {

        val fragments: ArrayList<Fragment> = ArrayList()
        val scannerFragment = ScannerFragment()
        fragments.add(BillFragment())
        fragments.add(scannerFragment)
        fragments.add(HotKeyFragment())
        fragments.add(HistoryFragment())

        val adapter = CustomPagerAdapter(fragments, supportFragmentManager)
        contentPageView.adapter = adapter
        contentTab.setupViewPager(contentPageView)
        contentTab.setTabChangeListener(object : AnimatedTabLayout.OnChangeListener {
            override fun onChanged(position: Int) {
                Log.e("onChanged", "" + position)
                if (position == 1) {
                    contentTab.background =
                        ContextCompat.getDrawable(this@MainActivity, R.color.colorBlack)
                    contentPageLayout.background =
                        ContextCompat.getDrawable(this@MainActivity, R.color.colorBlack)
                    scannerFragment.startCamera()
                } else {
                    contentTab.background =
                        ContextCompat.getDrawable(this@MainActivity, R.color.colorWhite)
                    contentPageLayout.background =
                        ContextCompat.getDrawable(this@MainActivity, R.color.colorDarkLight)
                    scannerFragment.stopCamera()
                }
            }
        })

//        val fragments: ArrayList<Fragment> = ArrayList()
//        val titles: ArrayList<String> = arrayListOf("บิล", "สแกน", "ไม่มีบาร์โค้ด", "ประวัติ")
//        val icons: ArrayList<Int> = arrayListOf(
//            R.drawable.ic_bill,
//            R.drawable.ic_scan,
//            R.drawable.ic_list,
//            R.drawable.ic_history
//        )
//
//
//        fragments.add(BillFragment())
//        fragments.add(ScannerFragment())
//        fragments.add(HotKeyFragment())
//        fragments.add(HistoryFragment())
//
//        val customAdapterPage = CustomPagerAdapter(fragments, supportFragmentManager)
//        contentTab.setupWithViewPager(contentPageView)
//        contentPageView.apply { adapter = customAdapterPage }
//        for (i in 0 until contentTab.tabCount) {
//            contentTab.getTabAt(i)?.setText(titles[i])?.setIcon(icons[i])
//        }
//
//        contentTab.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
//            override fun onTabReselected(tab: TabLayout.Tab?) {
//
//            }
//
//            override fun onTabUnselected(tab: TabLayout.Tab?) {
//                tab?.icon?.setTint(ContextCompat.getColor(this@MainActivity, R.color.colorWhite))
//            }
//
//            override fun onTabSelected(tab: TabLayout.Tab?) {
//                tab?.icon?.setTint(ContextCompat.getColor(this@MainActivity, R.color.colorWhite))
//                if (tab!!.position == 1) {
//                    contentTab.background =
//                        ContextCompat.getDrawable(this@MainActivity, R.color.colorBlack)
//                } else {
//                    contentTab.background =
//                        ContextCompat.getDrawable(this@MainActivity, R.color.colorPrimary)
//                }
//            }
//
//        })
        val shoppingBoard = ShoppingFragment()
        supportFragmentManager.beginTransaction().add(R.id.frameShop, shoppingBoard, "")
            .addToBackStack(null).commit()

    }

    private fun onEvent() {
        /*panalShowModal.setOnClickListener {
            openBottomSheetShoppingBoard()
        }*/

        swipePanelClose.setOnTouchListener { v, event ->
            setSwipingExpand(v, event)
            return@setOnTouchListener true
        }

        swipePanel.setOnTouchListener { v, event ->
            setSwipingExpand(v, event)
            return@setOnTouchListener true
        }
    }

    private fun setSwipingExpand(v: View, event: MotionEvent) {
        when (event.action) {
            MotionEvent.ACTION_MOVE -> {
                newX = event.x
                newY = event.y
                dX = newX - oldX
                dY = newY - oldY

                if (oldY > newY && newY < 0) {
                    val params = frameShop.layoutParams as ConstraintLayout.LayoutParams
                    frameShop.layoutParams.height = frameShop.height + abs(dY.toInt())
                    frameShop.layoutParams = params
                } else {
                    if (dY > 0 && newY > dY) {
                        val params = panalShowModal.layoutParams as ConstraintLayout.LayoutParams
                        panalShowModal.layoutParams.height =
                            ConstraintLayout.LayoutParams.WRAP_CONTENT
                        panalShowModal.layoutParams = params

                        val frameShopParams =
                            frameShop.layoutParams as ConstraintLayout.LayoutParams
                        frameShop.layoutParams.height = frameShop.height - abs(newY.toInt())
                        frameShop.layoutParams = frameShopParams
                        Log.e("h", (frameShop.height - abs(newY.toInt())).toString())
                    } else if (dY > 0) {
                        val params = frameShop.layoutParams as ConstraintLayout.LayoutParams
                        frameShop.layoutParams.height = frameShop.height - abs(dY.toInt())
                        frameShop.layoutParams = params
                    }
                }
            }
            MotionEvent.ACTION_DOWN -> {
                oldX = event.x
                oldY = event.y
            }
            MotionEvent.ACTION_UP -> {
                if (calScrollPercent(frameShop.height.toFloat())) {
                    val params = panalShowModal.layoutParams as ConstraintLayout.LayoutParams
                    panalShowModal.layoutParams.height = ConstraintLayout.LayoutParams.MATCH_PARENT
                    panalShowModal.layoutParams = params

                    val frameShopParams = frameShop.layoutParams as ConstraintLayout.LayoutParams
                    frameShop.layoutParams.height = 0
                    frameShop.layoutParams = frameShopParams
                    swipePanel.visibility = View.GONE
                    swipePanelClose.visibility = View.VISIBLE
                } else {
                    val params = panalShowModal.layoutParams as ConstraintLayout.LayoutParams
                    panalShowModal.layoutParams.height = ConstraintLayout.LayoutParams.WRAP_CONTENT
                    panalShowModal.layoutParams = params

                    val frameShopParams = frameShop.layoutParams as ConstraintLayout.LayoutParams
                    frameShop.layoutParams.height = 0
                    frameShop.layoutParams = frameShopParams
                    swipePanel.visibility = View.VISIBLE
                    swipePanelClose.visibility = View.GONE
                }
            }
        }
    }

    private fun calScrollPercent(cal: Float): Boolean {
        Log.e("cal", "$cal")
        Log.e("getScreenHeightSize", "${getScreenHeightSize() * 0.5}")
        return cal > getScreenHeightSize() * 0.5
    }

    private fun getScreenHeightSize(): Int {
        val display = this.windowManager.defaultDisplay
        val size = Point()
        display.getSize(size)
        return size.y
    }


    private fun openBottomSheetShoppingBoard() {
        val bottomSheetSummaryRetailSale = BottomShoppingBoard()
        bottomSheetSummaryRetailSale.show(
            supportFragmentManager, BottomShoppingBoard::class.java.simpleName
        )

        bottomSheetSummaryRetailSale.setOnModalEventListener(object :
            BottomShoppingBoard.ModalCallback {
            @SuppressLint("RestrictedApi")
            override fun onModalHide() {
                panalShowModal.visibility = View.VISIBLE
                val animation: Animation = AnimationUtils.loadAnimation(
                    applicationContext,
                    R.anim.slide_to_top
                )
                panalShowModal.animation = animation
            }

            @SuppressLint("RestrictedApi")
            override fun onModalShow() {
                panalShowModal.visibility = View.GONE
                val animation: Animation = AnimationUtils.loadAnimation(
                    applicationContext,
                    R.anim.slide_to_bottom
                )
                panalShowModal.animation = animation
            }

        })
    }

    private fun setDummyData() {
        for (i in 0..3) {
            val dummyItem = DummyContent.DummyItem(
                (i + 1).toLong(),
                "มาม่า",
                i + 2,
                6.00,
                "https://image.makewebeasy.net/makeweb/0/cVLsH6ZXs/DefaultData/8850987101021_1.jpg"
            )
            DummyContent.ITEMS.add(dummyItem)
        }
    }
}
